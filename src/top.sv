/*
    Tang Primer 20K 内蔵のLEDを１秒ごとに点滅する
 */
module top #(
    parameter CLOCK_LIMIT = 27_000_000  // Tang Primer 20K のクロックは 27Mhz
) (
    input   logic clock,
    output  logic led
);

// [math]::pow(2,24) < 27_000_000 < [math]::pow(2,25)
// https://qiita.com/vega77/items/9eeffd4f08435d1a490f を参考にbit幅を計算している
logic [$clog2(CLOCK_LIMIT +1) -1 : 0] count;

initial begin
    count   = 'd0;
    led     = 1'b0;
end

always_ff @(posedge clock) begin
    if (CLOCK_LIMIT == count) begin
        count   <= 'd0;
        led     <= ~led;
    end else begin
        count   <= count + 'd1;
    end    
end
endmodule
